//
//  AppDelegate.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//


// com.app.Draco-Support


import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import SwiftyJSON
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
    var objCustomTabBar = CustomTabbarVC()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
        //Redirect when click on push and login that time set true
        isScanScreenForIssue = true
        
        //Push notification
        
        FirebaseApp.configure()
        
        application.applicationIconBadgeNumber = 0
        Messaging.messaging().delegate = self
        
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.removeAllDeliveredNotifications()
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now(), execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
                
                if granted {
                    print("granted:==\(granted)")
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "grantedFalse"), object: nil)
                }
                
            }
        }
        else
        {
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
        //Map Integration---  AIzaSyC1WUEDlcogOOomYbsG-KJ-G2g7jxyxwyA
        GMSServices.provideAPIKey("AIzaSyC1WUEDlcogOOomYbsG-KJ-G2g7jxyxwyA")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

//MARK: - Usernotification delegate

extension AppDelegate
{
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        
        UserDefaults.standard.set(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("ERROR : \(error.localizedDescription)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let dict = JSON(notification.request.content.userInfo)
        
        print("Dict : \(JSON(dict))")
        /*
        if isRequestList == true
        {
             completionHandler([.alert,.badge,.sound])
        }
        else
        {
            completionHandler([.alert,.badge,.sound])
        }*/
        
         completionHandler([.alert,.badge,.sound])
        
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        
        let jsonDict = response.notification.request.content.userInfo
        
        print("JSON DICt - ",JSON(jsonDict))
        
        if getUserDetail("id") == ""
        {
            return
        }
        
        print("isScanScreenForIssue:\(isScanScreenForIssue)")
        
        if isScanScreenForIssue == true
        {
            
      //      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ScanScreenForIssue"), object: nil)
            
            appdelgate.objCustomTabBar = CustomTabbarVC()
            let navigationController = UINavigationController(rootViewController: appdelgate.objCustomTabBar)
            navigationController.popToRootViewController(animated: true)
            self.window?.rootViewController = navigationController
            navigationController.setNavigationBarHidden(true, animated: false)
        }
        else if isRequestList == true
        {
             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshRequestList"), object: nil)
        }
        else
        {
            appdelgate.objCustomTabBar.selectedIndex = 0
        }
        
        
        completionHandler()
    }
    
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    
    func connectToFcm() {
        // Won't connect since there is no token
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        
    }
    
}
