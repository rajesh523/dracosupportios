//
//  AdminLoginVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import FirebaseMessaging

class AdminLoginVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var vwPassword: CustomView!
   
    @IBOutlet weak var btnForgotPassword: UIButton!
    
    @IBOutlet weak var btnLogin: CustomButton!
    @IBOutlet weak var btnForSignUp: UIButton!
    
    
    @IBOutlet weak var imgViewCheckMark: UIImageView!
    @IBOutlet weak var lblSavePassword: UILabel!
    @IBOutlet weak var btnSavePassword: UIButton!
    
    
     //MARK: - Variable
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        
        self.checkLogin()
        self.navigationController?.navigationBar.isHidden = true
     //   setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Draco_Support_key"))
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
        
}

//MARK: - SetupUI

extension AdminLoginVc
{
    func setupUI()
    {
        
        imgViewCheckMark.image = UIImage(named: "ic_chak_off_box")
        
        if (Defaults.string(forKey: "imageCheckMark") == "ic_chak_on_box") {
            txtEmail.text = Defaults.string(forKey: "emailStore")
            txtPassword.text = Defaults.string(forKey: "passworldStore")
            let image = Defaults.string(forKey: "imageCheckMark")
            imgViewCheckMark.image = UIImage(named: image ?? "ic_chak_off_box")
        }
        
        self.lblSavePassword.textColor = UIColor.lightGray
        self.lblSavePassword.font = themeFont(size: 14, fontname: .regular)
        
        [txtEmail,txtPassword].forEach { (txt) in
            txt?.delegate = self
            txt?.leftPaddingView = 55
            txt?.setThemeTextFieldUI()
            
        }
        
        [vwEmail,vwPassword].forEach { (vw) in
            vw?.cornerRadius = (vw!.bounds.height)/2
            vw?.borderWidth = 1.0
            vw?.borderColors = UIColor.appthemeRedColor
            vw?.shadowColors = UIColor.lightGray
            vw?.shadowRadius = 2.0
            vw?.shadowOffset = CGSize(width: 1.0, height: 1.0)
            vw?.shadowOpacity = 0.8
        }
        
        txtEmail.placeholder = getCommonString(key: "Email_key")
        txtPassword.placeholder = getCommonString(key: "Password_key")
        
        btnForgotPassword.setTitle(getCommonString(key: "Forgot_Password_?_key"), for: .normal)
        btnForgotPassword.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnForgotPassword.setTitleColor(UIColor.lightGray, for: .normal)
        
        btnLogin.setThemeButtonUI()
        btnLogin.setTitle(getCommonString(key: "Login_key"), for: .normal)
        
        btnForSignUp.setTitle(getCommonString(key: "New_user_?_Sign_up_key"), for: .normal)
        btnForSignUp.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnForSignUp.setTitleColor(UIColor.lightGray, for: .normal)
        btnForSignUp.titleLabel?.textAlignment = .right
        
    }
}

//MARK: Other Function

extension AdminLoginVc {
    func checkLogin() {
        if getUserDetail("id") != ""
        {
            if (getUserDetail("role") == "1") {
                appdelgate.objCustomTabBar = CustomTabbarVC()
                self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
            }
            else if (getUserDetail("role") == "2") {
                appdelgate.objCustomTabBar = CustomTabbarVC()
                self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
            }
            else {
                let vc = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
//            appdelgate.objCustomTabBar = CustomTabbarVC()
//            self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
        }
//        else
//        {
//            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminLoginVc") as! AdminLoginVc
//            self.navigationController?.pushViewController(obj, animated: true)
//        }
    }
    
    func saveEmailPassword() {
        
        //save email/password
        if (imgViewCheckMark.image == UIImage(named: "ic_chak_on_box")) {
            Defaults.set(txtEmail.text, forKey: "emailStore")
            Defaults.set(txtPassword.text, forKey: "passworldStore")
            Defaults.set("ic_chak_on_box", forKey: "imageCheckMark")
        }
        else if (imgViewCheckMark.image == UIImage(named: "ic_chak_off_box")) {
            Defaults.removeObject(forKey: "emailStore")
            Defaults.removeObject(forKey: "passworldStore")
            Defaults.removeObject(forKey: "imageCheckMark")
        }
    }
}

//MARK: - IBAction

extension AdminLoginVc
{
    @IBAction func loginTapped(_ sender:UIButton)
    {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_email_key"))
        }
        else if (txtPassword.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_password_key"))
        }
        else if (txtPassword.text!.count) < 6
        {
            makeToast(strMessage: getCommonString(key:"Password_must_be_at_least_6_characters_long_key"))
        }
        else
        {
            self.saveEmailPassword()
            self.loginAPICalling()
        }
        
    }
    
    @IBAction func btnForgotPasswordTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ForgotPasswordVc") as! ForgotPasswordVc
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .crossDissolve
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnSignpTapped(_ sender: UIButton) {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "AdminSignupVc") as! AdminSignupVc
        self.navigationController?.pushViewController(obj, animated: true)
 
    }
    
    @IBAction func btnSavePassword(_ sender: Any) {
        
        if (imgViewCheckMark.image == UIImage(named: "ic_chak_off_box")) {
            imgViewCheckMark.image = UIImage(named: "ic_chak_on_box")
        }
        else {
            imgViewCheckMark.image = UIImage(named: "ic_chak_off_box")
        }
        
    }
    
}

//MARK: - UITextFieldDelegate

extension AdminLoginVc: UITextFieldDelegate
{

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
}

//MARK: - API calling

extension AdminLoginVc
{
    
    func loginAPICalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(loginURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "email" : txtEmail.text ?? "",
                         "password" : txtPassword.text ?? "",
                        "register_id" : Messaging.messaging().fcmToken ?? "",
                        "device_token" : Defaults.value(forKey: "device_token") as? String ?? "",
                        "device_type" : GlobalVariables.deviceType
                
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetails")
                        Defaults.synchronize()
                        
                        if data["role"].stringValue == "1" {
                            
                            appdelgate.objCustomTabBar = CustomTabbarVC()
                            appdelgate.objCustomTabBar.selectedIndex = 0
                            self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
                        }
                        else if data["role"].stringValue == "2" {
                            appdelgate.objCustomTabBar = CustomTabbarVC()
                            appdelgate.objCustomTabBar.selectedIndex = 0
                            self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
                        }
                        else {
                            let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.navigationController?.pushViewController(obj, animated: true)
                        }
                        
                        
//                    appdelgate.objCustomTabBar = CustomTabbarVC()
//                    self.navigationController?.pushViewController(appdelgate.objCustomTabBar, animated: false)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
}
