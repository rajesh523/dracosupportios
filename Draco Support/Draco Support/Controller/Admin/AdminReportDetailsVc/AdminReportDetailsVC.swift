//
//  AdminReportDetailsVC.swift
//  Draco Support
//
//  Created by Haresh Bhai on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import SwiftyJSON


class AdminReportDetailsVC: UIViewController {
    
    @IBOutlet weak var lblTitleTechName: UILabel!
    @IBOutlet weak var lblTechName: UILabel!
    
    @IBOutlet weak var lblTitleClientName: UILabel!
    @IBOutlet weak var lblClientName: UILabel!
    
    @IBOutlet weak var lblTitleDate: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var lblTitleProblem: UILabel!
    @IBOutlet weak var lblProblem: UILabel!
    
    @IBOutlet weak var lblTitleServiceType: UILabel!
    @IBOutlet weak var lblServiceType: UILabel!
    
    @IBOutlet weak var lblTitleServiceDate: UILabel!
    @IBOutlet weak var lblServiceDate: UILabel!
    
    @IBOutlet weak var lblTitleTimeSpent: UILabel!
    @IBOutlet weak var lblTimeSpent: UILabel!
    
    @IBOutlet weak var lblTitleServiceDetails: UILabel!
    @IBOutlet weak var lblServiceDetails: UILabel!
    
    @IBOutlet weak var lblTitleParts: UILabel!
    @IBOutlet weak var lblParts: UILabel!
    
    @IBOutlet weak var lblTitleNotes: UILabel!
    @IBOutlet weak var lblNotes: UILabel!

    @IBOutlet weak var vwProblem        : UIStackView!
    @IBOutlet weak var vwTimeSpent: UIStackView!
    @IBOutlet weak var vwServiceDetails : UIStackView!
    @IBOutlet weak var vwParts          : UIStackView!
    @IBOutlet weak var vwNotes          : UIStackView!
    
    
    //MARK: - Variable
    
    var dictReportData = JSON()
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "Report_Details_key"))
        self.setupData()
        self.setDetails()
        // Do any additional setup after loading the view.
    }
    
    func setupData() {
        self.setLabel(label: self.lblTitleTechName, key: "Technician_Name_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblTechName, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleClientName, key: "Client_Name_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblClientName, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleDate, key: "Date_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblDate, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleProblem, key: "Problem_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblProblem, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleServiceType, key: "Service_Type_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblServiceType, key: "", color: UIColor.lightGray, fontSize: 16)

        self.setLabel(label: self.lblTitleServiceDate, key: "Service_Date_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblServiceDate, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleTimeSpent, key: "Time_Spent_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblTimeSpent, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleServiceDetails, key: "Service_Details_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblServiceDetails, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleParts, key: "Parts_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblParts, key: "", color: UIColor.lightGray, fontSize: 16)
        
        self.setLabel(label: self.lblTitleNotes, key: "Notes_Request_key", color: UIColor.darkGray, fontSize: 16)
        self.setLabel(label: self.lblNotes, key: "", color: UIColor.lightGray, fontSize: 16)
    }
    
    func setDetails()
    {
        
        self.lblTechName.text = dictReportData["technician_name"].stringValue
        self.lblClientName.text = dictReportData["client_name"].stringValue
        
        self.lblDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd hh:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dictReportData["request_date"].stringValue)
        
        self.lblProblem.text = dictReportData["problem"].stringValue
        
        
        if dictReportData["service_type"].intValue == 0
        {
            self.lblServiceType.text = getCommonString(key: "On_Site_key")
        }
        else
        {
            self.lblServiceType.text = getCommonString(key: "Remote_key")
        }
        
        
        self.lblServiceDate.text = stringTodate(OrignalFormatter: "yyyy-MM-dd hh:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dictReportData["service_date"].stringValue)
        
        let timeSpent = self.dictReportData["time_spent"].intValue
        
        let min = timeSpent % 60
        let hrs = timeSpent / 60
        
        if min == 0
        {
            self.lblTimeSpent.text = "\(hrs) Hours"
        }
        else if hrs == 0
        {
            self.lblTimeSpent.text = "\(min) min"
        }
        else
        {
            self.lblTimeSpent.text = "\(hrs) Hours \(min) min"
        }
        
        // For problem detail label
        if self.dictReportData["problem"].stringValue == "" {
            self.vwProblem.isHidden = true
        }
        
        //For service detail label
        if self.dictReportData["service_detail"].stringValue == "" {
            self.vwServiceDetails.isHidden = true
        }
        else {
            self.vwServiceDetails.isHidden = false
            self.lblServiceDetails.text = self.dictReportData["service_detail"].stringValue
        }
        
        // For time_spent detail
        if self.dictReportData["time_spent"].stringValue == "" {
            self.vwTimeSpent.isHidden = true
        }
        
        //For parts label
        if self.dictReportData["parts"].stringValue == "" {
            self.vwParts.isHidden = true
        } else {
            self.vwParts.isHidden = false
            self.lblParts.text = self.dictReportData["parts"].stringValue
        }
        
        //For Notes/Requests label
        
        if self.dictReportData["notes"].stringValue == "" {
            self.vwNotes.isHidden = true
        } else {
            self.vwNotes.isHidden = false
            self.lblNotes.text = self.dictReportData["notes"].stringValue
        }
        
//        self.lblServiceDetails.text = self.dictReportData["service_detail"].stringValue
//
//        self.lblParts.text = self.dictReportData["parts"].stringValue
//        self.lblNotes.text = self.dictReportData["notes"].stringValue
        
    }
    
    func setLabel(label: UILabel, key: String, color: UIColor, fontSize:Float) {
        if key != "" {
            label.text = getCommonString(key: key)
        }
        label.textColor = color
        label.font = themeFont(size: fontSize, fontname: .regular)
    }


}
