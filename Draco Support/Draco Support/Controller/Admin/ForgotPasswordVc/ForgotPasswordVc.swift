//
//  ForgotPasswordVc.swift
//  Draco Support
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON


class ForgotPasswordVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var lblForgotPassword: UILabel!
    
    @IBOutlet weak var lblForgotPasswordMsg: UILabel!
    
    @IBOutlet weak var vwEmail: CustomView!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
        // Do any additional setup after loading the view.
    }

}


//MARK: - SetupUI
extension ForgotPasswordVc
{
    func setupUI()
    {
        lblForgotPassword.text = getCommonString(key: "Forgot_Password_key")
        lblForgotPassword.font = themeFont(size: 17, fontname: .regular)
        lblForgotPassword.textColor = UIColor.appThemeDarkGrayColor
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        txtEmail.delegate = self
        txtEmail.leftPaddingView = 55
        txtEmail.setThemeTextFieldUI()
        txtEmail.placeholder = getCommonString(key: "Email_key")
        
        vwEmail.cornerRadius = (vwEmail.bounds.height)/2
        vwEmail.borderWidth = 1.0
        vwEmail.borderColors = UIColor.appthemeRedColor
        vwEmail.shadowColors = UIColor.lightGray
        vwEmail.shadowRadius = 2.0
        vwEmail.shadowOffset = CGSize(width: 1.0, height: 1.0)
        vwEmail.shadowOpacity = 0.8

        lblForgotPasswordMsg.textColor = UIColor.lightGray
        lblForgotPasswordMsg.font = themeFont(size: 15, fontname: .regular)
        lblForgotPasswordMsg.text = getCommonString(key: "ForgotPassword_Msg_key")
        
    }
}

//MARK: - IBAction method
extension ForgotPasswordVc
{
    
    @IBAction func btnSubmitTapped(_ sender:UIButton)
    {
        
        if (txtEmail.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_email_key"))
        }
        else if !(isValidEmail(emailAddressString: txtEmail.text!))
        {
            makeToast(strMessage: getCommonString(key:"Please_enter_valid_email_key"))
        }
        else
        {
            self.forgotPasswordAPIcalling()
        }
 
    }
    
    @IBAction func btnCloseTapped(_ sender:UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
}

//MARK: - TextFieldDelegate

extension ForgotPasswordVc:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK: - API calling

extension ForgotPasswordVc
{
    
    func forgotPasswordAPIcalling()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(userURL)\(forgotPasswordURL)"
            
            print("URL: \(url)")
            
            let param = ["lang" : GlobalVariables.strLang,
                         "email" : txtEmail.text ?? ""
            ]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"]
                        
                        makeToast(strMessage: json["msg"].stringValue)
                        
                        self.dismiss(animated: false, completion: nil)
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
}
