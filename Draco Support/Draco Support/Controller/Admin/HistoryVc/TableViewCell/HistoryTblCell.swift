//
//  HistoryTblCell.swift
//  Draco Support
//
//  Created by Haresh on 26/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class HistoryTblCell: UITableViewCell {

    //MARK: - Outlet
    
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblTicketNumberValue: UILabel!
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblCompanyNameValue: UILabel!
    
    @IBOutlet weak var lblClientName: UILabel!
    @IBOutlet weak var lblClientNameValue: UILabel!
    
    @IBOutlet weak var lblSubmitedBy: UILabel!
    @IBOutlet weak var lblSubmitedByValue: UILabel!    
    
    @IBOutlet weak var lblSubmittedDate: UILabel!
    @IBOutlet weak var lblSubmittedDateValue: UILabel!
    
    @IBOutlet weak var lblTechName: UILabel!
    @IBOutlet weak var lblTechNameValue: UILabel!
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblEmailValue: UILabel!
    
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblDateTimeValue: UILabel!
    
    
    //MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTicketNumber,lblCompanyName,lblSubmitedBy,lblSubmittedDate,lblClientName,lblTechName,lblEmail,lblDateTime].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        lblTicketNumber.text = getCommonString(key: "Ticket_Number_Key")
        lblSubmittedDate.text = getCommonString(key: "Submitted_Date_key")
        lblCompanyName.text = getCommonString(key: "Company_Name_key")
        lblClientName.text = getCommonString(key: "Client_Name_key")
        lblTechName.text = getCommonString(key: "Technician_Name_key")
        lblEmail.text = getCommonString(key: "Email_:_key")
        lblDateTime.text = getCommonString(key: "Completed_date_Key")
        lblSubmitedBy.text = getCommonString(key: "Submitted_by_key")
        [lblTicketNumberValue,lblSubmittedDateValue,lblSubmitedByValue,lblCompanyNameValue,lblClientNameValue,lblTechNameValue,lblEmailValue,lblDateTimeValue].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
