//
//  RequestListVc.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class RequestListVc: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var tblRequestList: UITableView!
    
    //MARK: - Variable
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.appThemeDarkGrayColor
        
        return refreshControl
    }()
    
    var arrayRequestList : [JSON] = []
    var strErrorMessage = ""
    
    var arrayNewRequest : [JSON] = []
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblRequestList.register(UINib(nibName: "RequestTblCell", bundle: nil), forCellReuseIdentifier: "RequestTblCell")
        
        tblRequestList.tableFooterView = UIView()

        setupUI()
        
        print("getUserDetail(is_draco_admin):-- \(getUserDetail("is_draco_admin"))")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.requestListAPICalling(isShowLoader:)), name: NSNotification.Name(rawValue: "refreshRequestList"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        isRequestList = true
        
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Draco_Support_key"), isRightButtonHidden: false)
        setUpNavigationLeftQRCode()
        self.requestListAPICalling(isShowLoader: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        isRequestList = false
    }
    
    func setupUI()
    {
        
        
        tblRequestList.delegate = self
        tblRequestList.dataSource = self
        
        self.tblRequestList.addSubview(self.refreshControl)
    }

}


//MARK: - UITableview Delegate
extension RequestListVc : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
         if arrayNewRequest.count == 0
         {
             let lbl = UILabel()
             lbl.text = strErrorMessage
             lbl.textAlignment = NSTextAlignment.center
             lbl.textColor = UIColor.appthemeRedColor
             lbl.center = tableView.center
             tableView.backgroundView = lbl
            
             return 0
         }
         
         tableView.backgroundView = nil
         return arrayNewRequest.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestTblCell") as! RequestTblCell
        
        let dict = self.arrayNewRequest[indexPath.row]
        
        cell.lblTicketNumberValue.text = dict["ticketnumber"].stringValue
//        cell.lblSubmitteddateValue.text = dict["modified_at"].stringValue
        cell.lblNameValue.text = dict["client_name"].stringValue
        cell.lblCompanyNameValue.text = dict["company_name"].stringValue
        cell.lblDescriptionValue.text = dict["issue_description"].stringValue
        cell.lblNumberValue.text = dict["contact_no"].stringValue
        cell.btnDelete.isHidden = true
        
//        let date = stringTodate(OrignalFormatter: "yyyy-MM-dd hh:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["modified_at"].stringValue)
//        cell.lblSubmitteddateValue.text = "\(date)"
        
        cell.lblSubmitteddateValue.text = stringTodate(OrignalFormatter: "yyyy-MM-dd hh:mm:ss", YouWantFormatter: "d MMMM, yyyy", strDate: dict["modified_at"].stringValue)
        
        if getUserDetail("role") == "1" {
            cell.btnDelete.isHidden = false
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(sender:)), for: .touchUpInside)
        }
        cell.labInProgressHeightConstraint.constant = 0
        
//        if dict["status"].stringValue == "in_progress" {
//            cell.labInProgressHeightConstraint.constant = 20
//        } else {
//            cell.labInProgressHeightConstraint.constant = 0
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "RequestDetailsvc") as! RequestDetailsvc
        obj.dictIssueData = self.arrayNewRequest[indexPath.row]
//        obj.dictIssueData = self.arrayRequestList[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

//MARK: - Other method

extension RequestListVc
{
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        requestListAPICalling(isShowLoader: false)
        refreshControl.endRefreshing()
        
    }
    
    //MARK: - Delete Action
    @objc func btnDeleteAction(sender: UIButton) {
        let dic = self.arrayRequestList[sender.tag]
        print("dicdata:\(dic)")
        
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_you_want_to_delete_this_data?"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.deleteRowValue(issueId: dic["issue_id"].stringValue)
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
        //        self.deleteRowValue(issueId: dic["issue_id"].stringValue)
    }
    
    
}

//MARK: - API calling

extension RequestListVc
{
    
    @objc func requestListAPICalling(isShowLoader:Bool)
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(requesListURL)"
            
            print("URL: \(url)")
            
            let param = ["access_token" : getUserDetail("access_token"),
                         "user_id" : getUserDetail("id")
                
            ]
            
            print("param :\(param)")
            
            if isShowLoader
            {
                 self.showLoader()
            }
           
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
                if isShowLoader
                {
                    self.hideLoader()
                }
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayRequestList = []
                        self.arrayRequestList = data
                        
                        self.arrayNewRequest = []
                        self.arrayNewRequest = data.filter{$0["status"] == "todo"                }
                        print("Arry New Request:-- \(self.arrayNewRequest)")
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                         self.logoutAPICalling()
                    }
                    else
                    {
                        self.arrayNewRequest = []
                        self.strErrorMessage = json["msg"].stringValue
//                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                    self.tblRequestList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    @objc func deleteRowValue(issueId: String) {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicURLForClient)\(issueDetailURL)\(DeleteDataRequest)"
            
            print("URL: \(url)")
            
            let param = ["issue_id" : issueId,
                         "user_id" : getUserDetail("id")
                
            ]
            
            print("param :\(param)")
            
//            if isShowLoader
//            {
//                self.showLoader()
//            }
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:false, param: param) { (respones) in
                
//                if isShowLoader
//                {
//                    self.hideLoader()
//                }
                
                self.hideLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
//                        let data = json["data"].arrayValue
                        
                        self.requestListAPICalling(isShowLoader: true)
                        
//                        self.arrayRequestList = []
//                        self.arrayRequestList = data
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        self.logoutAPICalling()
                    }
                    else
                    {
                        self.strErrorMessage = json["msg"].stringValue
                        //                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    makeToast(strMessage: "Data delete successfully!!!")
//                    self.tblRequestList.reloadData()
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }

    }
    
}
