//
//  RequestTblCell.swift
//  Draco Support
//
//  Created by Haresh on 23/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit

class RequestTblCell: UITableViewCell {

    //MARK: - Outlet
    
    
    @IBOutlet weak var lblTicketNumber: UILabel!
    @IBOutlet weak var lblTicketNumberValue: UILabel!
    
    @IBOutlet weak var lblSubmittedDate: UILabel!
    @IBOutlet weak var lblSubmitteddateValue: UILabel!
    
    
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblCompanyNameValue: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNameValue: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblNumberValue: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblDescriptionValue: UILabel!
    
    @IBOutlet weak var btnDelete: CustomButton!
    
    @IBOutlet weak var labInProgressHeightConstraint: NSLayoutConstraint!
    
    
    
    //MARK: - Variable
    
    
    //MARK: - Viewlife cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTicketNumber,lblName,lblCompanyName,lblNumber,lblDescription, lblSubmittedDate].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        lblTicketNumber.text = getCommonString(key: "Ticket_Number_Key")
        lblCompanyName.text = getCommonString(key: "Company_Name_key")
        lblName.text = getCommonString(key: "Submitted_by_key")
        lblNumber.text = getCommonString(key: "Number_key")
        lblDescription.text = getCommonString(key: "Description_key")
        lblSubmittedDate.text = getCommonString(key: "Submitted_Date_key")
        
        [lblTicketNumberValue,lblNameValue,lblCompanyNameValue,lblNumberValue,lblDescriptionValue, lblSubmitteddateValue].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
