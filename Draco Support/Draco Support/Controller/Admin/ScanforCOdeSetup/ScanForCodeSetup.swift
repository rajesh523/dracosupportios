//
//  ScanForCodeSetup.swift
//  Draco Support
//
//  Created by Haresh on 29/03/19.
//  Copyright © 2019 YASH. All rights reserved.
//

import UIKit
import QRCodeReader
import AVFoundation
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON

class ScanForCodeSetup: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblDetailsMsg: UILabel!
    @IBOutlet weak var btnCustomer: UIButton!
    @IBOutlet weak var vwScanner: QRCodeReaderView!{
        didSet {
            vwScanner.setupComponents(with: QRCodeReaderViewControllerBuilder {
                $0.reader                 = reader
                $0.showTorchButton        = false
                $0.showSwitchCameraButton = false
                $0.showCancelButton       = false
                $0.showOverlayView        = false
                
                // $0.rectOfInterest         = CGRect(x: 0.0, y: 0.0, width: 0.6, height: 0.6)
            })
        }
    }
    
    @IBOutlet weak var txtQRCodeEnter: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    lazy var reader: QRCodeReader = QRCodeReader()
    
    var arrayCompanyList : [JSON] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        
        //CheckPermission
        guard checkScanPermissions(), !reader.isRunning else { return }
        reader.didFindCode = { result in
            print("Completion with result: \(result.value) of type \(result.metadataType)")
            
            print("result:\(result)")
            
            self.redirectToSubmitDetails(scanValue: result.value)
           
        }
        
        addDoneButtonOnKeyboard(textfield: txtQRCodeEnter)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "QR_Code_Setup_key"), isRightButtonHidden: false)
        setUpNavigationBarWhiteWithTitleAndBack(strTitle: getCommonString(key: "QR_Code_Setup_key"))
        
        reader.startScanning()
        getCompanyOrganizationList()
    }

    override func viewWillDisappear(_ animated: Bool) {
        reader.stopScanning()
    }
    
   
    
    
}


//MARK: - SetupUI

extension ScanForCodeSetup
{
    func setupUI()
    {
        self.vwMain.backgroundColor = UIColor.appThemeDarkGrayColor
        
        self.lblDetailsMsg.textColor = UIColor.white
        self.lblDetailsMsg.font = themeFont(size: 16, fontname: .regular)
        self.lblDetailsMsg.text = getCommonString(key: "Please_put_your_code_into_the_rectangle_view_finder_to_scan_key")
        
        btnCustomer.setThemeButtonUI()
        btnCustomer.setTitle(getCommonString(key: "Customer_key"), for: .normal)
        
        
        txtQRCodeEnter.delegate = self
        txtQRCodeEnter.placeholder = getCommonString(key: "Enter_QR_Code_key")
        txtQRCodeEnter.font = themeFont(size: 16, fontname: .regular)
        txtQRCodeEnter.textColor = UIColor.black
        txtQRCodeEnter.tintColor = UIColor.appthemeRedColor
        txtQRCodeEnter.backgroundColor = UIColor.white
        
        btnSubmit.setThemeButtonUI()
        btnSubmit.layer.cornerRadius = 5.0
        btnSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
    }
    
    func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            self.present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    func redirectToSubmitDetails(scanValue:String)
    {
        let obj = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "QRCodeSetupVc") as! QRCodeSetupVc
        obj.codeString = scanValue
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if (txtQRCodeEnter.text?.trimmingCharacters(in: .whitespaces))!.isEmpty
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_QR_code_key"))
        }
        else if (txtQRCodeEnter.text?.isNumeric == false)
        {
            makeToast(strMessage: getCommonString(key: "Please_enter_valid_QR_code_key"))
        }
        else
        {
//            getCompanyOrganizationList()
            redirectToSubmitDetails(scanValue: txtQRCodeEnter.text!)

        }
    }
    
    @IBAction func btnCustomerTapped(_ sender: UIButton) {
        let alertController = UIAlertController(title: getCommonString(key: "Draco_Support_key"), message: getCommonString(key: "Are_you_sure_want_to_move_customer_side_key"), preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            self.redirectToCustomerSide()
            
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func redirectToCustomerSide()
    {
        
        let vc  = GlobalVariables.mainStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let rearNavigation = UINavigationController(rootViewController: vc)
        appdelgate.window?.rootViewController = rearNavigation
        
    }
    
}

extension ScanForCodeSetup {
    
    func checkQRRegisterOrNot(qrString:String)
    {
        var jsonofScanQR = JSON()
        
        if self.arrayCompanyList.contains(where: { (json) -> Bool in
            
            if json["qr_code"].stringValue == qrString
            {
                jsonofScanQR = json
                return true
            }
            return false
        })
        {
//            self.redirectToSubmitDetails(qrScanData: jsonofScanQR)
        }
        else
        {
            makeToast(strMessage: getCommonString(key: "QRCode_is_not_registerrd_key"))
            self.reader.startScanning()
        }
    }
    
    
    
}


//MARK: - TextFIeld Delegate

extension ScanForCodeSetup :UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}


// MARK: - QRCodeReader Delegate Methods

extension ScanForCodeSetup
{
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capture to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - API calling

extension ScanForCodeSetup
{
    func getCompanyOrganizationList()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let url = "\(basicAdminURL)\(issueDetailURL)\(QRCodeDetails)"
            
            print("URL: \(url)")
            
            let param = [String:String]()
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().PostService(url: url,isLogin:true, param: param) { (respones) in
                
                self.hideLoader()
                
                self.reader.startScanning()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == GlobalVariables.strSuccessResponse
                    {
                        let data = json["data"].arrayValue
                        
                        self.arrayCompanyList = []
                        
                        self.arrayCompanyList = data
                        
                    }
                    else if json["flag"].stringValue == GlobalVariables.strAccessDenied
                    {
                        // self.logoutAPICalling()
                    }
                    else
                    {
                        makeToast(strMessage: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(strMessage: serverNotResponding)
                }
            }
        }
        else
        {
            makeToast(strMessage: networkMsg)
        }
        
    }
    
    
}
