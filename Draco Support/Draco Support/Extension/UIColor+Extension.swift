//
//  UIColor+Extension.swift
//  JoeToGo
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    static var appThemeDarkGrayColor: UIColor { return UIColor.init(red: 68/255, green: 68/255, blue: 68/255, alpha: 1.0) }

    static var appthemeRedColor: UIColor { return UIColor.init(red: 169/255, green: 30/255, blue: 33/255, alpha: 1.0) }
}
