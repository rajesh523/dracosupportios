//
//  CommonService.swift
//  JoeToGo
//
//  Created by Haresh on 22/03/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService
{
    func PostService(url: String,isLogin:Bool,param: [String:String],completion:@escaping(Result<JSON>)-> ())
    {
        print("url - ",url)
        
        let strConvertedURL = url.encodedURLString()
        
        var headers: HTTPHeaders? = nil
        
        if isLogin == false
        {
            headers = ["Authorization":"Basic \(getUserDetail("access_token"))"]
        }
        
        print("headers:==\(headers ?? [:])")
        
        request(strConvertedURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: headers).authenticate(user:kBasicUsername, password: kBasicPassword).responseSwiftyJSON(completionHandler:
            {

                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
//                    print("StatusCode : \(statusCode)")

                    if(statusCode == 500)
                    {
                        completion($0.result)
                    }else if(statusCode != nil)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: "")
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: "")
                    completion($0.result)
                }
        })
    }
    
    
    func GetService(url: String,completion:@escaping(Result<JSON>)-> ())
    {
        let strURL = url.encodeString()
        print("strURL - ",strURL)
        
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).authenticate(user:kBasicUsername, password: kBasicPassword).responseSwiftyJSON(completionHandler:
            {
                print("Request Response - ",$0.result)
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
//                    print("StatusCode : \(statusCode)")
                    if(statusCode == 500)
                    {
                        completion($0.result)
                    }else if(statusCode != nil)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        makeToast(strMessage: "")
                        completion($0.result)
                    }
                }else
                {
                    makeToast(strMessage: "")
                    completion($0.result)
                }
        })
    }
    
    //TODO: message changes
}
